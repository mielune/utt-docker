# UTT Docker image

A docker image for [Ultimate Time Tracker](https://github.com/larose/utt).

Thank's to [Mathieu Larose](https://github.com/larose) for this tool :)

## Build

Run this command:
```zsh
docker build -t utt .
```

If you are behind a proxy, you can use this command:
```zsh
docker build --build-arg HTTP_PROXY=$HTTP_PROXY --build-arg HTTPS_PROXY=$HTTPS_PROXY --build-arg http_proxy=$HTTP_PROXY --build-arg https_proxy=$HTTPS_PROXY -t utt .
```

## Usage

You can use this docker image like a local UTT's install.

See [UTT Commands](https://github.com/larose/utt#commands) for usage details.

You can run UTT report like this:
```zsh
docker run -v ~/utt/:/data -u=nobody utt utt --data /data/myutt.data --timezone 'Europe/Paris' report
```

or add an alias to your env
```zsh
utt="docker run -v ~/utt/:/data -u=nobody utt utt --data /data/myutt.data --timezone 'Europe/Paris' "
```
to run utt with 3 letters:
```
utt hello
utt add "Build a new app"
utt report
```

## Contributors

- Mielune <https://gitlab.com/mielune>

## Thanks to

- Mathieu Larose <mathieu@mathieularose.com>
- Add all others UTT contributors

## License

utt-docker is released under the GPLv3. See the LICENSE file for details.
